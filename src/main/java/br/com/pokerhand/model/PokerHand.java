package br.com.pokerhand.model;

import java.util.ArrayList;
import java.util.List;

import br.com.pokerhand.types.PokerRules;
import br.com.pokerhand.types.Resultado;

/**
 * @author Matheus Guedes Viana
 * @https://gitlab.com/users/guedesiw/projects
 *
 */
public class PokerHand {
	private List<Carta> cartas = new ArrayList<Carta>();
	private PokerRules pokerHand;
	
	public PokerHand(String cartas) {
		String[] arrayCartas = cartas.split(" ");
		for(String representacaoCarta : arrayCartas) {
			this.cartas.add(new Carta(representacaoCarta));
		}
		this.pokerHand = PokerRules.getPokerRules(this);
	}
	
	public PokerRules getPokerHand() {
		return pokerHand;
	}
	public void setPokerHand(PokerRules pokerHand) {
		this.pokerHand = pokerHand;
	}
	public List<Carta> getCartas() {
		return cartas;
	}
	public void setCartas(List<Carta> cartas) {
		this.cartas = cartas;
	}
	
	public Resultado compareWith(PokerHand hand) {
		Integer valor;
		if(this.pokerHand.equals(hand.getPokerHand())) {
			valor = somarPesosDasCartas(this.getCartas()).compareTo(somarPesosDasCartas(hand.getCartas()));
		}else {
			valor = this.pokerHand.getPeso().compareTo(hand.getPokerHand().getPeso());
		}
		switch(valor) {
		case -1:
			return Resultado.LOSS;
		case 0:
			return Resultado.DRAW;
		case 1:
			return Resultado.WIN;
		default:
			throw new RuntimeException("Resultado não esperado!");
		}
	}
	
	public Integer somarPesosDasCartas(List<Carta> cartas) {
		Integer valor = 0;
		for(Carta carta : cartas) {
			valor += carta.getCarta().getPeso();
		}
		return valor;
	}
}
