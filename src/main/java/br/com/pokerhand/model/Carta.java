package br.com.pokerhand.model;

import br.com.pokerhand.types.Cartas;
import br.com.pokerhand.types.Naipes;

/**
 * @author Matheus Guedes Viana
 * @https://gitlab.com/users/guedesiw/projects
 *
 */
public class Carta {
	private Cartas carta;
	private Naipes naipe;
	
	public Carta(String represencacaoCarta) {
		this.carta = Cartas.getCarta(represencacaoCarta);
		this.naipe = Naipes.getNaipe(represencacaoCarta);
	}
	
	public Naipes getNaipe() {
		return naipe;
	}
	public void setNaipe(Naipes naipe) {
		this.naipe = naipe;
	}
	public Cartas getCarta() {
		return carta;
	}
	public void setCarta(Cartas carta) {
		this.carta = carta;
	}
	
}
