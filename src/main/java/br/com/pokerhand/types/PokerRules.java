package br.com.pokerhand.types;

/**
 * @author Matheus Guedes Viana
 * @https://gitlab.com/users/guedesiw/projects
 *
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.pokerhand.model.Carta;
import br.com.pokerhand.model.PokerHand;

public enum PokerRules {
	ROYAL_FLUSH(10,true),
	STRAIGHT_FLUSH(9,false),
	QUADRA(8,true),
	FULL_HOUSE(7,false),
	FLUSH(6,false),
	SEQUENCIA(5,false),
	TRINCA(4,false),
	DOIS_PARES(3,false),
	UM_PAR(2,false),
	CARTA_ALTA(1,false);
	
	private Integer peso;
	private Boolean comparaPesoNaipes;
	
	private PokerRules(Integer peso, Boolean comparaPesoNaipes) {
		this.peso = peso;
		this.comparaPesoNaipes = comparaPesoNaipes;
	}
	
	public static PokerRules getPokerRules(PokerHand hand) {
		if(isRoyalFlush(hand.getCartas())) {
			return PokerRules.ROYAL_FLUSH;
		}
		if(isStraightFlush(hand.getCartas())) {
			return PokerRules.STRAIGHT_FLUSH;
		}
		if(isQuadra(hand.getCartas())) {
			return PokerRules.QUADRA;
		}
		if(isFullHouse(hand.getCartas())) {
			return PokerRules.FULL_HOUSE;
		}
		if(isFlush(hand.getCartas())) {
			return PokerRules.FLUSH;
		}
		if(isSequencia(hand.getCartas())) {
			return PokerRules.SEQUENCIA;
		}
		if(isTrinca(hand.getCartas())) {
			return PokerRules.TRINCA;
		}
		if(isDoisPares(hand.getCartas())) {
			return PokerRules.DOIS_PARES;
		}
		if(isUmPar(hand.getCartas())) {
			return PokerRules.UM_PAR;
		}
		return PokerRules.CARTA_ALTA;
	}
	
	private static Boolean isRoyalFlush(List<Carta> cartas){
		List<Cartas> listaDeCartas = new ArrayList<Cartas>();
		Naipes naipePadrao = null;
		for(Carta carta : cartas) {
			if(naipePadrao == null) {
				naipePadrao = carta.getNaipe();
			}else if(!naipePadrao.equals(carta.getNaipe())) {
				return false;
			}
			if(carta.getCarta().equals(Cartas.DEZ) && !listaDeCartas.contains(Cartas.DEZ)) {
				listaDeCartas.add(Cartas.DEZ);
				continue;
			}
			if(carta.getCarta().equals(Cartas.VALETE) && !listaDeCartas.contains(Cartas.VALETE)) {
				listaDeCartas.add(Cartas.VALETE);
				continue;
			}
			if(carta.getCarta().equals(Cartas.RAINHA) && !listaDeCartas.contains(Cartas.RAINHA)) {
				listaDeCartas.add(Cartas.RAINHA);
				continue;
			}
			if(carta.getCarta().equals(Cartas.REI) && !listaDeCartas.contains(Cartas.REI)) {
				listaDeCartas.add(Cartas.REI);
				continue;
			}
			if(carta.getCarta().equals(Cartas.ACE) && !listaDeCartas.contains(Cartas.ACE)) {
				listaDeCartas.add(Cartas.ACE);
				continue;
			}
			return false;
		}
		return true;
	}
	
	private static List<Integer> getPesosPorNaipe(List<Carta> cartas, Boolean avaliarNaipe, Boolean mesmoNaipe){
		Naipes naipePadrao = null;
		List<Integer> listaDePesos = new ArrayList<Integer>();
		Boolean naipeDiferente = false;
		for(Carta carta : cartas) {
			if(avaliarNaipe) {
				if(naipePadrao == null) {
					naipePadrao = carta.getNaipe();
				}
				if(!mesmoNaipe || naipePadrao.equals(carta.getNaipe())) {
					if(!naipePadrao.equals(carta.getNaipe())) {
						naipeDiferente = true;
					}
					listaDePesos.add(carta.getCarta().getPeso());
				}else {
					return new ArrayList<Integer>();
				}
			}else {
				listaDePesos.add(carta.getCarta().getPeso());
			}
		}
		if(avaliarNaipe && !mesmoNaipe && !naipeDiferente) {
			return new ArrayList<Integer>();
		}
		Collections.sort(listaDePesos);
		return listaDePesos;
	}
	
	private static Boolean avaliarPesos(List<Integer> listaDePesos, Integer adicionar) {
		Integer pesoAuxiliar = 0;
		if(listaDePesos.size() == 0) {
			return false;
		}
		for(Integer peso : listaDePesos) {
			if(pesoAuxiliar == 0) {
				pesoAuxiliar = peso;
			}else if((adicionar == 1 && (pesoAuxiliar + adicionar) == peso) ||
					 (adicionar == 2 && (pesoAuxiliar + adicionar) >= peso)) {
				pesoAuxiliar += adicionar;
			}else {
				return false;
			}
		}
		return true;
	}
	
	private static Boolean avaliarPorQuantidade(List<Carta> cartas, Integer quantidade1, Integer quantidade2){
		List<Integer> listaDePesos = getPesosPorNaipe(cartas, false, false);
		Integer pesoPadrao1 = 0, contadorPesoPadrao1 = 0, pesoPadrao2 = 0, contadorPesoPadrao2 = 0;
		if(listaDePesos.size() == 0) {
			return false;
		}
		for(Integer peso : listaDePesos) {
			if(pesoPadrao1 == 0) {
				pesoPadrao1 = peso;
				contadorPesoPadrao1++;
			}else if(pesoPadrao1 == peso) {
				contadorPesoPadrao1++;
			}else if(pesoPadrao2 == 0) {
				pesoPadrao2 = peso;
				contadorPesoPadrao2++;
			}else if(pesoPadrao2 == peso) {
				contadorPesoPadrao2++;
			}else if(contadorPesoPadrao1 == quantidade1 || contadorPesoPadrao1 == quantidade2) {
				if((contadorPesoPadrao1 + contadorPesoPadrao2) != (quantidade1 + quantidade2)) {
					pesoPadrao2 = peso;
					contadorPesoPadrao2 = 1;
				}
			}else if(contadorPesoPadrao2 == quantidade1 || contadorPesoPadrao2 == quantidade2) {
				if((contadorPesoPadrao1 + contadorPesoPadrao2) != (quantidade1 + quantidade2)) {
					pesoPadrao1 = peso;
					contadorPesoPadrao1 = 1;
				}
			}
		}
		return (contadorPesoPadrao1 == quantidade1 && contadorPesoPadrao2 == quantidade2) ||
			   (contadorPesoPadrao2 == quantidade1 && contadorPesoPadrao1 == quantidade2);
	}
	
	private static Boolean getFlushPorSequencia(List<Carta> cartas, Integer adicionar, Boolean verificarTamanho) {
		if(verificarTamanho) {
			return getPesosPorNaipe(cartas, true, true).size() == 5;
		}
		return avaliarPesos(getPesosPorNaipe(cartas, true, true), adicionar);
	}
	
	private static Boolean isStraightFlush(List<Carta> cartas){
		return getFlushPorSequencia(cartas, 1, false);
	}
	
	private static Boolean isQuadra(List<Carta> cartas){
		return avaliarPorQuantidade(cartas, 1, 4);
	}
	
	private static Boolean isFullHouse(List<Carta> cartas){
		return avaliarPorQuantidade(cartas, 2, 3);
	}
	
	private static Boolean isFlush(List<Carta> cartas){
		return getFlushPorSequencia(cartas, 2, true);
	}
	
	private static Boolean isSequencia(List<Carta> cartas){
		return avaliarPesos(getPesosPorNaipe(cartas, true, false), 1);
	}
	
	private static Boolean isTrinca(List<Carta> cartas){
		return avaliarPorQuantidade(cartas, 1, 3);
	}
	
	private static Boolean isDoisPares(List<Carta> cartas){
		return avaliarPorQuantidade(cartas, 2, 2);
	}
	
	private static Boolean isUmPar(List<Carta> cartas){
		return avaliarPorQuantidade(cartas, 1, 2);
	}

	public Integer getPeso() {
		return peso;
	}

	public Boolean getComparaPesoNaipes() {
		return comparaPesoNaipes;
	}
}
