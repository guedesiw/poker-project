package br.com.pokerhand.types;

/**
 * @author Matheus Guedes Viana
 * @https://gitlab.com/users/guedesiw/projects
 *
 */
public enum Resultado {
	WIN, LOSS, DRAW;
}
