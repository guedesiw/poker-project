package br.com.pokerhand.types;

/**
 * @author Matheus Guedes Viana
 * @https://gitlab.com/users/guedesiw/projects
 *
 */
public enum Cartas {
	DOIS("2", 1),
	TRES("3", 2),
	QUATRO("4", 3),
	CINCO("5", 4),
	SEIS("6", 5),
	SETE("7", 6),
	OITO("8", 7),
	NOVE("9", 8),
	DEZ("T", 9),
	VALETE("J", 10),
	RAINHA("Q", 11),
	REI("K", 12),
	ACE("A", 13);
	
	private String representacao;
	private Integer peso;
	
	private Cartas(String representacao, Integer peso) {
		this.representacao = representacao;
		this.peso = peso;
	}

	public String getRepresentacao() {
		return representacao;
	}

	public Integer getPeso() {
		return peso;
	}
	
	public static Cartas getCarta(String representacao) {
		for(Cartas carta : Cartas.values()) {
			if(carta.getRepresentacao().equals(Character.toString(representacao.charAt(0)))){
				return carta;
			}
		}
		throw new RuntimeException("Não existe a carta: "+representacao.charAt(0));
	}
}
