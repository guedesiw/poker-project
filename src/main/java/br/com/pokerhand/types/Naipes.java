package br.com.pokerhand.types;

/**
 * @author Matheus Guedes Viana
 * @https://gitlab.com/users/guedesiw/projects
 *
 */
public enum Naipes {
	ESPADAS("S"), COPAS("H"), OUROS("D"), PAUS("C");
	
	private String representacao;
	
	private Naipes(String representacao) {
		this.representacao = representacao;
	}

	public String getRepresentacao() {
		return representacao;
	}
	
	public static Naipes getNaipe(String representacao) {
		for(Naipes naipe : Naipes.values()) {
			if(naipe.getRepresentacao().equals(Character.toString(representacao.charAt(1)))){
				return naipe;
			}
		}
		throw new RuntimeException("Não existe o naipe: "+representacao.charAt(1));
	}
}
