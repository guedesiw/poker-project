package br.com.pokerhand;

import br.com.pokerhand.model.PokerHand;
import br.com.pokerhand.types.Resultado;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * @author Matheus Guedes Viana
 * @https://gitlab.com/users/guedesiw/projects
 *
 */
public class TestPokerHand 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TestPokerHand( String testName )
    {
        super( testName );
        testePokerHand("9C TC JC QC KC", "9C 9H 5C 5H AC", Resultado.WIN, 1);
        testePokerHand("TC TH 5C 5H KH", "9C 9H 5C 5H AC", Resultado.WIN, 2);
        testePokerHand("7H 7C QC JS TS", "7D 7C JS TS 6D", Resultado.WIN, 3);
        testePokerHand("5S 5D 8C 7S 6H", "7D 7S 5S 5D JS", Resultado.LOSS, 4);
        testePokerHand("5S 5D 8C 7S 6H", "7D 7S 5S 5D JS", Resultado.LOSS, 5);
        testePokerHand("AS AD KD 7C 3D", "AD AH KD 7C 4S", Resultado.LOSS, 6);
        testePokerHand("TS JS QS KS AS", "AC AH AS AS KS", Resultado.WIN, 7);
        testePokerHand("TS JS QS KS AS", "TC JS QC KS AC", Resultado.WIN, 8);
        testePokerHand("TS JS QS KS AS", "QH QS QC AS 8H", Resultado.WIN, 9);
        testePokerHand("AC AH AS AS KS", "TC JS QC KS AC", Resultado.WIN, 10);
        testePokerHand("AC AH AS AS KS", "QH QS QC AS 8H", Resultado.WIN, 11);
        testePokerHand("TC JS QC KS AC", "QH QS QC AS 8H", Resultado.WIN, 12);
        testePokerHand("7H 8H 9H TH JH", "JH JC JS JD TH", Resultado.WIN, 13);
        testePokerHand("7H 8H 9H TH JH", "4H 5H 9H TH JH", Resultado.WIN, 14);
        testePokerHand("7H 8H 9H TH JH", "TS TH TD JH JD", Resultado.WIN, 15);
        testePokerHand("7H 8H 9H TH JH", "JH JD TH TC 4C", Resultado.WIN, 16);
        testePokerHand("JH JC JS JD TH", "4H 5H 9H TH JH", Resultado.WIN, 17);
        testePokerHand("JH JC JS JD TH", "7C 8S 9H TH JH", Resultado.WIN, 18);
        testePokerHand("JH JC JS JD TH", "TS TH TD JH JD", Resultado.WIN, 19);
        testePokerHand("JH JC JS JD TH", "JH JD TH TC 4C", Resultado.WIN, 20);
        testePokerHand("4H 5H 9H TH JH", "7C 8S 9H TH JH", Resultado.WIN, 21);
        testePokerHand("4H 5H 9H TH JH", "TS TH TD JH JD", Resultado.LOSS, 22);
        testePokerHand("4H 5H 9H TH JH", "JH JD TH TC 4C", Resultado.WIN, 23);
        testePokerHand("7C 8S 9H TH JH", "TS TH TD JH JD", Resultado.LOSS, 24);
        testePokerHand("7C 8S 9H TH JH", "JH JD TH TC 4C", Resultado.WIN, 25);
        testePokerHand("TS TH TD JH JD", "JH JD TH TC 4C", Resultado.WIN, 26);
        testePokerHand("2S 3H 4D 5H 6D", "5H 6D 7H 8C 9C", Resultado.LOSS, 27);
        testePokerHand("2S 3H 4H 5H 6D", "2S 3H 4D 5H 6C", Resultado.DRAW, 28);
        testePokerHand("2H 3H 4H 5H 7H", "2D 3D 4D 5D 8D", Resultado.LOSS, 29);
        testePokerHand("2S 2H 2D 5H 6D", "5H 5D 5H 8C 9C", Resultado.LOSS, 30);
        testePokerHand("2H 3H 4H 5H 6H", "5H 6H 7H 8H 9H", Resultado.LOSS, 31);
        testePokerHand("TH JH QH KH AH", "TC JC QC KC AC", Resultado.DRAW, 32);
        testePokerHand("TH TH TH TH AS", "9C 9C 9C 9C 2S", Resultado.WIN, 33);
        testePokerHand("TH TH TH AH AS", "9C 9C 9C 2C 2S", Resultado.WIN, 34);
        testePokerHand("2H 4H 6H 8H AS", "3C 5C 6C 8C JS", Resultado.WIN, 35);
        testePokerHand("2H 2H 5H AH AS", "2C 2C 6C AC AS", Resultado.LOSS, 37);
        testePokerHand("2H 3C 3D 3S 6H", "2C 3D 4D 5C 6C", Resultado.LOSS, 38);
        testePokerHand("AS 2S 5S 8S QS", "KS JS 5S 8S QS", Resultado.LOSS, 39);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( TestPokerHand.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    public void testePokerHand(String representacaoHand1, String representacaoHand2, Resultado resultadoEsperado, Integer numeroTeste) {
    	PokerHand hand1 = new PokerHand(representacaoHand1);
    	PokerHand hand2 = new PokerHand(representacaoHand2);
    	Resultado resultado = hand1.compareWith(hand2);
    	assertEquals(resultadoEsperado, resultado);
    	System.out.println("Teste "+numeroTeste+" OK!");
    }
}
